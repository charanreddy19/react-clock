import React from 'react'
import clock from './clock.png'

class Header extends React.Component {
    render(){
      return(
        <div id="header">
        <h1 id="headerText">
          <img src={clock} alt="" id="clockImg"/>
          React Clock
        </h1>
        </div>
      )
    }
}

export  default Header