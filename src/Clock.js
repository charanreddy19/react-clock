import React from 'react'
import calender from './calender.png'

class Clock extends React.Component{
    constructor() {
        super()
        this.state={
            year:new Date().getFullYear().toString().length>1?new Date().getFullYear():'0'+new Date().getFullYear(),
            month:new Date().getMonth(),
            date:new Date().getDate().toString().length>1?new Date().getDate():'0'+new Date().getDate(),
            hours:new Date().getHours().toString().length>1?new Date().getHours():'0'+new Date().getHours(),
            minutes:new Date().getMinutes().toString().length>1?new Date().getMinutes():'0'+new Date().getMinutes(),
            seconds:new Date().getSeconds().toString().length>1?new Date().getSeconds():'0'+new Date().getSeconds(),
            day:new Date().getDay(),
            second:new Date().getSeconds(),
            display:true
        }
        this.changeHandler=this.changeHandler.bind(this)
    }
    componentDidMount() {
        setInterval(()=>{
            this.setState({
                year:new Date().getFullYear().toString().length>1?new Date().getFullYear():'0'+new Date().getFullYear(),
                month:new Date().getMonth(),
                date:new Date().getDate().toString().length>1?new Date().getDate():'0'+new Date().getDate(),
                hours:new Date().getHours().toString().length>1?new Date().getHours():'0'+new Date().getHours(),
                minutes:new Date().getMinutes().toString().length>1?new Date().getMinutes():'0'+new Date().getMinutes(),
                seconds:new Date().getSeconds().toString().length>1?new Date().getSeconds():'0'+new Date().getSeconds(),
                day:new Date().getDay(),
                second:new Date().getSeconds()
            })
        },1000)
      }

    changeHandler(event) {
        console.log()
        this.setState({
            display:event.target.checked
        })
    }
    
    render() {
        let days=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
        let months=["Jan","Feb","Mar","Apr","May","Jun","July","Aug","Sep","Oct","Nov","Dec",]
        return (
        <div id="clockDiv">
            <div id="section">
            {this.state.display?<label for="checkbox" class="check">
            <input type="checkbox" onChange={this.changeHandler} checked={this.state.display} id="checkbox"/>
            <span id="checkSpan"></span>
            </label>:<label for="checkbox" class="checks">
            <input type="checkbox" onChange={this.changeHandler} checked={this.state.display} id="checkbox"/>
            <span id="checkSpan"></span>
            </label>}
            <img src={calender} alt="" id="calenderImg"/>
            </div>
            <div id="timeDiv">
            <h1 id="time">{this.state.hours}:{this.state.minutes}:{this.state.seconds}</h1>
            {this.state.display?<h2 id="date">{days[this.state.day]} {this.state.date} {months[this.state.month]} {this.state.year}</h2>:null}
            </div>
        </div>
        )
    }
}

export default Clock