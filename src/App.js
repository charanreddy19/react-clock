import './App.css';
import React from 'react'
import Header from './Header.js'
import Clock from './Clock.js'


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          <Header/>
          <Clock/>
        </p>
      </header>
    </div>
  )
}

export default App;